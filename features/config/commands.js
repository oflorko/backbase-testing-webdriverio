import ComputerDatabase from '../../pageObjects/ComputerDatabase';

const computerDatabasePage = new ComputerDatabase(browser);

module.exports = {
	textIsVisible: (text) => {
		expect(`//*[contains(text(), "${text}")]`).to.be.visible();
	},

	clickTheText: (text) => {
		browser.waitForVisible(`//*[contains(text(), "${text}")]`);
		browser.click(`//*[contains(text(), "${text}")]`);
	}
};