Feature: Add new computer
  
  Background:
    Given I am on the add a computer page

  Scenario Outline: I can add a computer with "<description>"
    Given I set the computer name to "<name>"
    And I set the Introduced date to "<introduced>"
    And I set the Discontinued date to "<discontinued>"
    And I set the Company to "<company>"
    When I click Create this computer
    Then Alert message should contain "Done! Computer <name> has been created"
    And I filter by "<name>"
    And I expect search result "<name>" to be <expected>

    Examples:
      | description                        | name                 | introduced | discontinued | company | expected |
      | Empty Introduced Date              | Empty Introduced     |            | 2020-09-05   | 1       | true     |
      | Empty Discontinued Date            | Empty Discontinued   | 2018-09-05 |              | 2       | true     |
      | Empty Company                      | Empty Company        | 2018-09-05 | 2020-09-05   |         | true     |
      | Empty Dates                        | Empty Dates          |            |              | 3       | true     |
      | Empty DiscontinuedDate And Company | Empty DiscontCompany | 2018-09-05 |              |         | true     |
      | Empty IntroducedDate And Company   | Empty IntrodCompany  |            | 2020-09-05   |         | true     |
      | Only Name                          | Only Name            |            |              |         | true     |
    
  Scenario: I can't add a computer without a name
    Given I set the computer name to ""
    When I click Create this computer

  Scenario Outline: I can add a new computer with "<description>"
    Given I set the computer name to "<name>"
    And I set the Introduced date to "<introduced>"
    And I set the Discontinued date to "<discontinued>"
    And I set the Company to "43"
    When I click Create this computer
    And I am on the computer database page
    And I filter by "<name>"
    And I expect search result "<name>" to be <expected>

    Examples:
      | description                 | name                  | introduced  | discontinued | expected |
      | Normal Dates                | Normal Dates          | 2015-11-18  | 2016-11-18   | true     |
      | Zero Year                   | Zero Year             | 0-1-1       | 9999-12-12   | true     |
      | Intro DateFormat dd-MM-yyyy | DateFormat dd-MM-yyyy | 31-12-2000  |              | false    |
      | Disc DateFormat dd-MM-yyyy  | DateFormat dd-MM-yyyy |             | 31-12-2000   | false    |
      | Intro Text Month            | Text Month            | 2015-Nov-18 |              | false    |
      | Disc Intro Text Month       | Intro Text Month      |             | 2015-Nov-18  | false    |
      | Intro Negative Year         | Negative Year         | -1-11-11    |              | false    |
      | Disc Negative Year          | Negative Year         |             | -1-11-11     | false    |
      | Intro Wrong Delimiter       | Wrong Delimiter       | 2000/1/1    |              | false    |
      | Disc Wrong Delimiter        | Wrong Delimiter       |             | 2000/1/1     | false    |
      | Intro Text Date             | Text Date             | text        |              | false    |
      | Intro Text Date             | Text Date             |             | text         | false    |
      | Intro DateFormat dd-MM-yyyy | DateFormat MM-dd-yyyy | 12-31-2000  |              | false    |
      | Disc DateFormat dd-MM-yyyy  | DateFormat MM-dd-yyyy |             | 12-31-2000   | false    |
      | Intro Space Delimiter       | Space Delimiter       | 2000 1 1    |              | false    |
      | Disc Space Delimiter        | Space Delimiter       |             | 2000 1 1     | false    |
      | Intro 30 Feb                | 30 Feb                | 2012-2-30   |              | false    |
      | Disc 30 Feb                 | 30 Feb                |             | 2012-2-30    | false    |