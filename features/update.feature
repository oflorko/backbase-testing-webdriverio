Feature: Computer update testing

  Background:
    Given I am on the computer database page

  Scenario: I can edit a computer name
    Given I create a new computer with the name "itIsAVeryUniqueNameToEdit"
    And I filter by "itIsAVeryUniqueNameToEdit"
    And I click the text "itIsAVeryUniqueNameToEdit"
    And I set the computer name to "itIsAVeryUniqueNameToEdit!!"
    When I click Save this computer
    Then Alert message should contain "Done! Computer itIsAVeryUniqueNameToEdit!! has been updated"

  Scenario: I can edit an introduced date
    Given I filter by "itIsAVeryUniqueNameToEdit"
    And I click the text "itIsAVeryUniqueNameToEdit"
    And I set the Introduced date to "1900-01-01"
    When I click Save this computer
    Then Alert message should contain "Done!"
    And I filter by "itIsAVeryUniqueNameToEdit"
    And I see text "01 Jan 1900"

  Scenario: I can edit a discontinued date
    Given I filter by "itIsAVeryUniqueNameToEdit"
    And I click the text "itIsAVeryUniqueNameToEdit"
    And I set the Discontinued date to "2999-12-31"
    When I click Save this computer
    Then Alert message should contain "Done!"
    And I filter by "itIsAVeryUniqueNameToEdit"
    And I see text "31 Dec 2999"

  Scenario: I can change a company
    And I filter by "itIsAVeryUniqueNameToEdit"
    And I click the text "itIsAVeryUniqueNameToEdit"
    And I set the Company to "24"
    When I click Save this computer
    Then Alert message should contain "Done!"
    And I filter by "itIsAVeryUniqueNameToEdit"
    And I see text "Nintendo"

  Scenario: I can cancel editing
    Given I create a new computer with the name "itIsAVeryUniqueNameToEdit"
    And I filter by "itIsAVeryUniqueNameToEdit"
    And I click the text "itIsAVeryUniqueNameToEdit"
    And I set the computer name to "AbsolutelyAnotherName"
    When I click the text "Cancel"
    And I filter by "AbsolutelyAnotherName"
    Then I expect search result "AbsolutelyAnotherName" to be false
