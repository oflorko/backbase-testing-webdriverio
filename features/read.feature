Feature: Test filtering scenarios

  Scenario Outline: Filtering <description>
    Given I am on the computer database page
    When I filter by "<value>"
    Then I expect search result "<value>" to be <expected>

    Examples:
      | description              | value                 | expected |
      | by text (first boundry)  | Acer Extensa 5220     | true     |
      | by text (second boundry) | lenovo thinkpad z61p  | true     |
      | by number                | 61                    | true     |
      | by symbol                | +                     | true     |
      | by not existing entry    | sdjljksvjkl78jhd      | false    |
