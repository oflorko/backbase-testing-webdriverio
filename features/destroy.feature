Feature: Computer removal testing

  Background:
    Given I am on the computer database page

  Scenario: I can remove a computer
    Given I create a new computer with the name "itIsAVeryUniqueNameToRemove"
    And I filter by "itIsAVeryUniqueNameToRemove"
    And I click the text "itIsAVeryUniqueNameToRemove"
    When I click Delete this computer
    Then Alert message should contain "Done! Computer has been deleted"
    
  Scenario: I can cancel the computer removal
    Given I create a new computer with the name "itIsAVeryUniqueNameToKeep"
    And I filter by "itIsAVeryUniqueNameToKeep"
    And I click the text "itIsAVeryUniqueNameToKeep"
    When I click the text "Cancel"
    Then I am on the computer database page
    And I filter by "itIsAVeryUniqueNameToKeep"
    And I expect search result "itIsAVeryUniqueNameToKeep" to be true