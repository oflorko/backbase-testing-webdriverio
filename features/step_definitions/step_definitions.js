import { Given, When, Then } from 'cucumber';
import ComputerDatabase from '../../pageObjects/ComputerDatabase';
import AddComputer from '../../pageObjects/AddComputer';
import EditComputer from '../../pageObjects/EditComputer';

const computerDatabasePage = new ComputerDatabase(browser);
const addComputerPage = new AddComputer(browser);
const editComputerPage = new EditComputer(browser);

Given(/^I am on the computer database page$/, () => {
	computerDatabasePage.open();
	expect(computerDatabasePage.addNewBtn.isVisible()).to.be.true;
});

When(/^I filter by "(.*)"$/, (name) => {
	computerDatabasePage.filterByName(name);
});

Then(/^I expect search result "(.*)" to be (.*)$/, (value, expectedResult) => {
	computerDatabasePage.verifyFiltering(value, expectedResult);
});

Given(/^I am on the add a computer page$/, () => {
	addComputerPage.open();
	expect(addComputerPage.submitBtn.isVisible()).to.be.true;
});

Given(/^I set the computer name to "(.*)"$/, (name) => {
	addComputerPage.setComputerNameTo(name);
});

Given(/^I set the Introduced date to "(.*)"$/, (date) => {
	addComputerPage.setIntroductionDateTo(date);
});

Given(/^I set the Discontinued date to "(.*)"$/, (date) => {
	addComputerPage.setDiscontinuedDateTo(date);
});

Given(/^I set the Company to "(.*)"$/, (company) => {
	addComputerPage.setCompanyTo(company);
});

When(/^I click Create this computer$/, () => {
	addComputerPage.createThisComputer();
});

Then(/^Alert message should contain "(.*)"$/, (msg) => {
	computerDatabasePage.verifyAlertMessage(msg);
});

Then(/^I see text "(.*)"$/, (text) => {
	browser.textIsVisible(text);
});

Given(/^I create a new computer with the name "(.*)"$/, (name) => {
	addComputerPage.open();
	addComputerPage.setComputerNameTo(name);
	addComputerPage.createThisComputer();
});

Given(/^I click the text "(.*)"$/, (text) => {
	browser.clickTheText(text);
});

When(/^I click Delete this computer$/, () => {
	editComputerPage.deleteThisComputer();
});

When(/^I click Save this computer$/, () => {
	editComputerPage.saveChanges();
});