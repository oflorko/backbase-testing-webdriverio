class EditComputer {
	constructor(browser) {
		this.browser = browser;
	}

	get deleteBtn()	{ return $('input.btn.danger') 	};
	get submitBtn() { return $('input.btn.primary')	};

	deleteThisComputer() {
		this.deleteBtn.waitForVisible();
		this.deleteBtn.click();
	}

	saveChanges() {
		this.submitBtn.waitForVisible();
		this.submitBtn.click();
	}
}

module.exports = EditComputer;
