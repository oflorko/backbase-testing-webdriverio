class AddComputer {
	constructor(browser) {
		this.browser = browser;
	}

	get header() 								{ return $('.topbar');					};
	get pageContainer() 				{ return $('#main'); 						};
	get computerNameInput()			{ return $('#name');						};
	get introducedDateInput()		{ return $('#introduced'); 			};
	get discontinuedDateInput()	{ return $('#discontinued');		};
	get conpanySelect()					{ return $('#company');					};
	get submitBtn()							{ return $('input.btn.primary') };

	open() {
		browser.url('/computers/new');
		this.header.waitForVisible();
		this.pageContainer.waitForVisible();
	}

	setComputerNameTo(name) {
		this.computerNameInput.waitForVisible();
		this.computerNameInput.clearElement();
		this.computerNameInput.setValue(name);
	}

	setIntroductionDateTo(date) {
		this.introducedDateInput.waitForVisible();
		this.introducedDateInput.clearElement();
		this.introducedDateInput.setValue(date);
	}

	setDiscontinuedDateTo(date) {
		this.discontinuedDateInput.waitForVisible();
		this.discontinuedDateInput.clearElement();
		this.discontinuedDateInput.setValue(date);
	}

	setCompanyTo(company) {
		this.conpanySelect.waitForVisible();
		this.conpanySelect.selectByValue(company);
	}

	createThisComputer() {
		this.submitBtn.waitForVisible();
		this.submitBtn.click();
	}
}

module.exports = AddComputer;
