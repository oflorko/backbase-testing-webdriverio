class ComputerDatabase {
	constructor(browser) {
		this.browser = browser;
	}

	get header() 					{ return $('.topbar'); 									};
	get pageContainer() 	{ return $('#main'); 										};
	get searchBox() 			{ return $('#searchbox'); 							};
	get searchSubmit() 		{ return $('#searchsubmit');						};
	get addNewBtn() 			{ return $('#add'); 										};
	get links() 					{ return $('tbody tr a'); 							};
	get alert()						{ return $('div.alert-message.warning') };

	open() {
		browser.url('/computers');
		this.header.waitForVisible();
		this.pageContainer.waitForVisible();
	}

	filterByName(name) {
		this.searchBox.waitForVisible();
		this.searchBox.clearElement();
		this.searchBox.setValue(name);
		this.searchSubmit.click();
	}

	verifyFiltering(name, result) {
		// Convert string to boolean
		result = (result === 'true');
		if(result) {
			assert.ok(this.links.getText().includes(name));
		} else {
			// Element should not exist
			expect(this.links.isExisting()).to.equal(false);
		}
	}

	verifyAlertMessage(msg) {
		// Alert message is visible
		expect(this.alert.isVisible()).to.equal(true);
		assert.ok(this.alert.getText().includes(msg));
	}
}

module.exports = ComputerDatabase;
