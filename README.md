# WebdriverIO Introduction

[WebdriverIO](http://webdriver.io/) is a mature WebDriver framework that allows you
to run tests on desktop browsers and even on native mobile apps.

Reasons why I chose WebdriverIO:
1. Super easy to configure - has a simple installation menu and generates
ready to configuration.
2. Many frameworks and plugins are available.
3. Supports mobile testing (always nice to have).
4. Supports BDT out of the box.
5. Big community of users, easy to get help.

## How to start
Please make sure that you have [Node.js](https://nodejs.org/en/) installed.
Go to project root in terminal and run the command:
```shell
$ npm install
```
This will download all necessary dependencies and store then in _node_modules_ folder.

## Test results
To demonstrate nice reporting solution I have done what I should not - commited
the report, I just want to save you some time. Also there are three failing tests to
demonstrate reporting feature.
Please run in terminal from the root project folder following command:
```
$ npx allure open
```
It will start for you the web server to represent the test report.
![Allure report](./Allure_screenshot.png)


## Test cases description
I used Gherkin language to describe manual tests because I do not like to do
a double work and have two test tracking systems at same time. Cucumber feature
files are just perfect place to store the tests, also they have all
required specifications.

Please check the _*.feature_ files in **./features/** folder.

**./features/step_descriptions/** contains steps implementations. Because there
are not that many steps yet I placed them on one file.

**./features/config/commands.js** has all the methods that are not related to
the page objects.

Test cases that I implemented are far away from being perfect, if I had more time
I would play more with boundary conditions. Also we should keep the balance in which
test cases to add to the UI testing to not make our solution two heavy.
Many of those test cases should be API or Unit tests, I wanted to experiment
with Cucumber flexibility and see how much code I need to write to cover them all.

## How to run tests
WebdriverIO downloads and installs all the necessary dependencies for you.
You don't need to configure a Selenium hub separately. All is included.

Please run in terminal in the root of project:
```
$ npm run test
```
It will start selenium server and run the tests automatically.
